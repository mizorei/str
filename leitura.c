//#define DEBUGTIMER
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* The rate at which the LED D0 will flash */
#define LED_FLASH_PERIOD_MS 	( 250 / portTICK_RATE_MS )
#define INTERF_TASK_PERIOD_MS 	( 3000 / portTICK_RATE_MS )
#define INTERF_LOAD_MS          ( 1000 )

/* Priorities of the demo application tasks (high numb. -> high prio.) */
#define LED_FLASH_PRIORITY	( tskIDLE_PRIORITY + 4 )
#define INTERF_PRIORITY	        ( tskIDLE_PRIORITY + 3 )

#define READ_ADC_MS          (100/portTICK_RATE_MS)
#define READ_PRIORITY       ( tskIDLE_PRIORITY + 4 )

#define PROCESS_FIFO_MS          (1000/portTICK_RATE_MS)
#define PROCESS_PRIORITY       ( tskIDLE_PRIORITY + 3 )

#define CLIENT_MS       ( 2000/portTICK_RATE_MS )
#define CLIENT_PRIORITY       ( tskIDLE_PRIORITY + 2 )

#define	MaxPross	15
#define MaxClient       45

/*
 * Prototypes and tasks
 */
char msg1[] = "Qt: ";
char msg2[] = " Vl:";
char m[140];

struct msg{
	int quantidade;
	int val[MaxClient];
};

struct msg mensagem;

void reverse(char s[])
{
	int i, j;
	char c;

	for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}

void itoa_str(int n, char s[])
{
	int i, sign;

	if ((sign = n) < 0)  /* record sign */
		n = -n;          /* make n positive */
	i = 0;
	do {       /* generate digits in reverse order */
		s[i++] = n % 10 + '0';   /* get next digit */
	} while ((n /= 10) > 0);     /* delete it */
	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';
	reverse(s);
}


int main(void){
	int i;
	char a[11];
	char *k;
	mensagem.quantidade=20;
	while(1){
		
		mensagem.val[0]=16010;
		for (i=1;i<20;i++){
			mensagem.val[i]=mensagem.val[i-1]+20;
		}

		strcat(m,msg1);
		itoa_str(mensagem.quantidade , a);
		strcat(m,a);
		strcat(m,msg2); 
		for(i=0;i<mensagem.quantidade;i++){
			itoa_str(mensagem.val[i],a);
			k=m;
			k= k+strlen(m);
			*k=' ';
			strcat(m,a);
		}
	
		printf("%s\n",m);
		memset(m,0,sizeof m);
	}
}
