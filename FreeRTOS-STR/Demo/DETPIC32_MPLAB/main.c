/*
 * Paulo Pedreiras, Aug/2014
 *
 * FREERTOS demo for DETPIC32 board
 * - Creates two periodic tasks
 * - One togles Led D0, other is a long task
 * - When interefering task has higher priority shows interference (led
 *       does not blink at the right rate
 *
 * Environment:
 * - MPLAB 1.51
 * - XC32 V1.2
 * - FreeRTOS V8.0.1
 *
 *Bruno Teixeira n� 65739
 * Joao Freitas n� 65073
 */

/* Generic includes*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <p32xxxx.h>
#include <plib.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"

#include "queue.h"
/* Hardware specific includes. */
#include "ConfigPerformance.h"
#include <limits.h>
//#define DEBUGTIMER    //este define tem de tar activo para o debuging dos tempos
//#define DEBUG1        //define para tirar os tempos da main task
//#define DEBUG2        //define para tirar os tempos da task process
//#define DEBUG3        //define para tirar os tempos da task client
/* The rate at which the LED D0 will flash */
//#define LED_FLASH_PERIOD_MS 	( 250 / portTICK_RATE_MS )
//#define INTERF_TASK_PERIOD_MS 	( 3000 / portTICK_RATE_MS )
//#define INTERF_LOAD_MS          ( 1000 )
/* Priorities of the demo application tasks (high numb. -> high prio.) */
//#define LED_FLASH_PRIORITY	( tskIDLE_PRIORITY + 4 )
//#define INTERF_PRIORITY	        ( tskIDLE_PRIORITY + 3 )

#define READ_ADC_MS          (1/portTICK_RATE_MS)
#define READ_PRIORITY       ( tskIDLE_PRIORITY + 4 )

#define PROCESS_FIFO_MS          (6/portTICK_RATE_MS)
#define PROCESS_PRIORITY       ( tskIDLE_PRIORITY + 3 )

#define CLIENT_MS       ( 12/portTICK_RATE_MS )
#define CLIENT_PRIORITY       ( tskIDLE_PRIORITY + 2 )

#define	MaxPross	11
#define MaxClient     23  

/*
 * Prototypes and tasks
 */

/*global variables*/
QueueHandle_t xQueueProc, xQueueClient;
xTaskHandle taskLer;
xTaskHandle taskPro;
xTaskHandle taskManda;

#ifdef DEBUGTIMER

#define DebugPriority ( tskIDLE_PRIORITY + 1 )
#define DEBUG_MS       ( 10000/portTICK_RATE_MS )
xTaskHandle taskTermite;
QueueHandle_t xQueueTimes;
char task1[] = "task1 ";
char task2[] = "task2 ";
char task3[] = "task3 ";
char MAX[] = " max: ";
char MIN[] = " min: ";
char COR[] = "cor: ";
unsigned int lastTicks = 0;
unsigned int firstTicks = 0;
unsigned int maxTicks = 0;
unsigned int minTicks = ULONG_MAX;
unsigned int currentTicks = 0;


#endif
int xStatus;
char msg1[] = "Qt: ";
char msg2[] = " Vl:"; //array de chars auxiliar para indicar os valores
char m[150]; //array de mensagens a ser lido

struct msg { //estrutura que contem a quantidade a ser enviada e os respectivos valores a serem enviados
    int quantidade;
    int val[MaxClient];
};
struct msg mensagem;

static void prvSetupHardware(void);
void itoa_str(int n, char s[]);
//funcao que ira ler da adc e escrever numa queueu os valores em raw

void readAdcTask(void *pvParam) {
    portTickType xLastWakeTime;
    int res;
    xLastWakeTime = xTaskGetTickCount();
    for (;;) {
        vTaskDelayUntil(&xLastWakeTime, READ_ADC_MS);
#ifdef DEBUG1
        //sugestao do prof -> usar o readcoretimer
        firstTicks = ReadCoreTimer();
#endif
        IFS1bits.AD1IF = 0; // Reset interrupt flag
        AD1CON1bits.ASAM = 1; // Start conversion
        /* tirar o tempo de conversao
           esta informacao so estara aqui presente para 
           incluir no relatorio depois disso apagar*/
        while (IFS1bits.AD1IF == 0); // Wait fo EOC     //isto so esta a ler um valor... inserir outros para termos uma media?
        res = ADC1BUF0; // acrescentar os outros buffs

        xStatus = xQueueSend(xQueueProc, (&res), 0); //escreve na queue para ser processada

#ifdef DEBUG1
        //sugestao do prof -> usar o readcoretimer
        lastTicks = ReadCoreTimer();
        currentTicks = lastTicks - firstTicks;
        if (maxTicks < currentTicks) {
            maxTicks = currentTicks;
        }
        if (minTicks > currentTicks) {
            minTicks = currentTicks;
        }
        xQueueSend(xQueueTimes, (&currentTicks), portMAX_DELAY);
        xQueueSend(xQueueTimes, (&maxTicks), portMAX_DELAY);
        xQueueSend(xQueueTimes, (&minTicks), portMAX_DELAY);

#endif                
        //vTaskDelay(READ_ADC_MS);
    }
}
//ler da queue nao processada processar a informacao e inserir na queue para o cliente

void fifoProcess(void *pvParam) {
    int sizeOfQueue;
    int aux;
    int xStat;
    portTickType xLastWakeTime;
    xLastWakeTime = xTaskGetTickCount();

    for (;;) {
        vTaskDelayUntil(&xLastWakeTime, PROCESS_FIFO_MS);

#ifdef DEBUG2
        //sugestao do prof -> usar o readcoretimer
        vTaskPrioritySet( &taskLer, PROCESS_PRIORITY );
        vTaskPrioritySet( &taskPro, READ_PRIORITY );
        firstTicks = ReadCoreTimer();
#endif        
        
        sizeOfQueue = uxQueueSpacesAvailable(xQueueProc);
        if (sizeOfQueue == MaxPross) {
        } else {
            //ler o valor da fila de processamento e converter para tensao	
            //sizeOfQueue = MaxPross - sizeOfQueue; //calcular o espaco ocupado
            xStat = xQueueReceive(xQueueProc, (&aux), 0);
            //sizeOfQueue--;
            while (xStat == pdPASS ){// && sizeOfQueue > 0) {
                aux = aux * 33; //actualiza os valores para milivolts
                xQueueSend(xQueueClient, (&aux), 0); //escreve o valor actualizado para a fifo do cliente
                xStat = xQueueReceive(xQueueProc, (&aux), 0); //le o proximo valor a ser processado
              //  sizeOfQueue--;
            }
        }
#ifdef DEBUG2
        //sugestao do prof -> usar o readcoretimer
        lastTicks = ReadCoreTimer();
        currentTicks = lastTicks - firstTicks;
        if (maxTicks < currentTicks) {
            maxTicks = currentTicks;
        }
        if (minTicks > currentTicks) {
            minTicks = currentTicks;
        }
        xQueueSend(xQueueTimes, (&currentTicks), portMAX_DELAY);
        xQueueSend(xQueueTimes, (&maxTicks), portMAX_DELAY);
        xQueueSend(xQueueTimes, (&minTicks), portMAX_DELAY);
#endif
        //vTaskDelay(PROCESS_FIFO_MS);
    }

}
//funcao retirada da internet
void reverse(char s[]) {
    int i, j;
    char c;

    for (i = 0, j = strlen(s) - 1; i < j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}
//funcao retirada da internet
void itoa_str(int n, char s[]) {
    int i, sign;
    if ((sign = n) < 0) /* record sign */
        n = -n; /* make n positive */
    i = 0;
    do { /* generate digits in reverse order */
        s[i++] = n % 10 + '0'; /* get next digit */
    } while ((n /= 10) > 0); /* delete it */
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);
}

void sendMessage(void) {
    int i;
    unsigned int ii;
    char *k;
    char a[11];
    	strcat(m,msg1);
    	itoa_str(mensagem.quantidade , a);
    	strcat(m,a);
    	strcat(m,msg2); 
    for (i = 0; i < mensagem.quantidade; i++) {
        itoa_str(mensagem.val[i], a);
        k = m;
        k = k + strlen(m);
        *k = ' ';
        strcat(m, a);
    }
    for (ii = 0; ii < strlen(m); ii++) {
        _mon_putc(m[ii]);
    }
    _mon_putc('\n');
    memset(m, 0, sizeof (m));

}
//funcao que escreve na estrutura a ser enviada para o cliente
void sendToClient(void *pvParam) {
    int sizeOf;
    int cli;
    int i;
    int aux;
    int xst;
    portTickType xLastWakeTime;
    xLastWakeTime = xTaskGetTickCount();
    for (;;) {
        vTaskDelayUntil(&xLastWakeTime, CLIENT_MS);
#ifdef DEBUG3
       vTaskPrioritySet( &taskLer, READ_PRIORITY );
        vTaskPrioritySet( &taskManda, READ_PRIORITY );
        //sugestao do prof -> usar o readcoretimer
        firstTicks = ReadCoreTimer();
#endif
        sizeOf = uxQueueSpacesAvailable(xQueueClient);
        if (sizeOf == MaxClient) {
        } else {
//            sizeOf = MaxClient-sizeOf;
            xst = xQueueReceive(xQueueClient, (&cli), 0);
          //&& sizeofqueue +/- tamanho =0;
            for(i=0; xst==pdPASS ; i++){
           // while ((xst == pdPASS) && (sizeOfQueue > 0)) {
                mensagem.val[i] = cli;
                xst= xQueueReceive(xQueueClient, (&cli), 0);
            }
            mensagem.quantidade = i;
            sendMessage(); //imprimir a estrutura para o cliente
            //}
        }
#ifdef DEBUG3
        //sugestao do prof -> usar o readcoretimer
        lastTicks = ReadCoreTimer();
        currentTicks = lastTicks - firstTicks;
        if (maxTicks < currentTicks) {
            maxTicks = currentTicks;
        }
        if (minTicks > currentTicks) {
            minTicks = currentTicks;
        }
        xQueueSend(xQueueTimes, (&currentTicks), portMAX_DELAY);
        xQueueSend(xQueueTimes, (&maxTicks), portMAX_DELAY);
        xQueueSend(xQueueTimes, (&minTicks), portMAX_DELAY);
#endif
        //	vTaskDelay(CLIENT_MS);
    }
}
#ifdef DEBUGTIMER

void DebugTimer(void *pvParam) {
    int firstRun = 0;
    char aux[100];
    unsigned int i;
    unsigned int l;
    int val = 0;
    //int aux2;   //o que vai ser escrito
    int xStat;
    for (;;) {
        if (firstRun == 0) {
            firstRun = 1;
        } else {
            /*inserir delete das task!!*/
            vTaskDelete(taskLer);
            vTaskDelete(taskPro);
            vTaskDelete(taskManda);

            xStat = xQueueReceive(xQueueTimes, (&l), portMAX_DELAY);
            while (xStat == pdPASS) {
                itoa_str(l, aux);
                if (val == 0) {
                    _mon_putc('c');
                    _mon_putc('u');
                    _mon_putc('r');
                    _mon_putc(' ');
                    val = 1;
                } else if (val == 1) {
                    _mon_putc(' ');

                    _mon_putc('M');
                    _mon_putc('a');
                    _mon_putc('x');
                    _mon_putc(' ');
                    val = 2;
                } else if (val == 2) {
                    _mon_putc(' ');

                    _mon_putc('M');
                    _mon_putc('i');
                    _mon_putc('n');
                    _mon_putc(' ');
                    val = 3;
                }
                for (i = 0; i < strlen(aux); i++) {
                    _mon_putc(aux[i]);
                }

                if (val == 3) {
                    _mon_putc('\n');
                    val = 0;
                }
                memset(aux, 0, sizeof (aux));

                xStat = xQueueReceive(xQueueTimes, (&l), portMAX_DELAY);
            }
            vTaskDelete(taskTermite);
        }
        vTaskDelay(DEBUG_MS);
    }
}
#endif
/*initialization of ADC pin*/
void initADC(void) {
    // Performance optimization (flash access time, enable instruct and data cache,... and PBClock setup
    DDPCONbits.JTAGEN = 0;
    // Initialize ADC module
    // Polling mode, AN0 as input
    // Generic part
    AD1CON1bits.SSRC = 7; // Internal counter ends sampling and starts conversion
    AD1CON1bits.CLRASAM = 1; //Stop conversion when 1st A/D converter interrupt is generated and clears ASAM bit automatically
    AD1CON1bits.FORM = 0; // Integer 16 bit output format
    AD1CON2bits.VCFG = 0; // VR+=AVdd; VR-=AVss

    AD1CON2bits.SMPI = 0; // Number (+1) of consecutive conversions, stored in ADC1BUF0...ADCBUF{SMPI}

    AD1CON3bits.ADRC = 1; // ADC uses internal RC clock
    AD1CON3bits.SAMC = 16; // Sample time is 16TAD ( TAD = 100ns)
    // Set AN0 as input
    AD1CHSbits.CH0SA = 0; // Select AN0 as input for A/D converter
    TRISBbits.TRISB0 = 1; // AN0 in input mode
    AD1PCFGbits.PCFG0 = 0; // AN0 as analog input
    // Enable module
    AD1CON1bits.ON = 1; // Enable A/D module (This must be the ***last instruction of configuration phase***)

}
/*
 * Create the demo tasks then start the scheduler.
 */
int main(void) {

    /* Prepare the hardware to run this demo. */
    prvSetupHardware();

    /*Criacao de duas queues*/
    initADC(); //inicializacao da ADC
    xQueueProc = xQueueCreate(MaxPross, sizeof (unsigned long));
    xQueueClient = xQueueCreate(MaxClient, sizeof (unsigned long));


#ifdef DEBUGTIMER
    xQueueTimes = xQueueCreate(1000, sizeof (unsigned long));
#endif
    /* Create the tasks defined within this file. */
    /* Finally start the scheduler. */
    xTaskCreate(readAdcTask, (const signed char * const) "ADC", configMINIMAL_STACK_SIZE, NULL, READ_PRIORITY, &taskLer);
    xTaskCreate(fifoProcess, (const signed char * const) "PROSS", configMINIMAL_STACK_SIZE, NULL, PROCESS_PRIORITY, &taskPro);
    xTaskCreate(sendToClient, (const signed char * const) "CLIENT", configMINIMAL_STACK_SIZE, NULL, CLIENT_PRIORITY, &taskManda);
#ifdef DEBUGTIMER
    xTaskCreate(DebugTimer, (const signed char * const) "Debug", configMINIMAL_STACK_SIZE, NULL, DebugPriority, &taskTermite);
#endif

    vTaskStartScheduler();

    /* Will only reach here if there is insufficient heap available to start
       the scheduler. */
    return 0;
}
/*-----------------------------------------------------------*/

// Function replacements to redirect stdin/stdout to USART1
// These functions are called by printf(), scanf(), ...

void _mon_putc(char c) {
    while (U1STAbits.UTXBF); // Wait till buffer available (TX Buffer Full)
    U1TXREG = c; // Put char in Tx buffer
    return;
}

static void prvSetupHardware(void) {
    /* Configure the hardware for maximum performance. */
    vHardwareConfigurePerformance();
    mOSCSetPBDIV(OSC_PB_DIV_2); // This is necessary since SYSTEMConfigPerformance defaults FPBDIV to DIV_1
    /* Setup to use the external interrupt controller. */
    vHardwareUseMultiVectoredInterrupts();
    portDISABLE_INTERRUPTS();
}

/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook(void) {
    /* vApplicationMallocFailedHook() will only be called if
       configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
       function that will get called if a call to pvPortMalloc() fails.
       pvPortMalloc() is called internally by the kernel whenever a task, queue,
       timer or semaphore is created.  It is also called by various parts of the
       demo application.  If heap_1.c or heap_2.c are used, then the size of the
       heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
       FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
       to query the size of free heap space that remains (although it does not
       provide information on how the remaining heap might be fragmented). */
    taskDISABLE_INTERRUPTS();
    for (;;);
}

/*-----------------------------------------------------------*/

void vApplicationIdleHook(void) {
    /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
       to 1 in FreeRTOSConfig.h.  It will be called on each iteration of the idle
       task.  It is essential that code added to this hook function never attempts
       to block in any way (for example, call xQueueReceive() with a block time
       specified, or call vTaskDelay()).  If the application makes use of the
       vTaskDelete() API function (as this demo application does) then it is also
       important that vApplicationIdleHook() is permitted to return to its calling
       function, because it is the responsibility of the idle task to clean up
       memory allocated by the kernel to any task that has since been deleted. */
}

/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook(TaskHandle_t pxTask, char *pcTaskName) {
    (void) pcTaskName;
    (void) pxTask;

    /* Run time task stack overflow checking is performed if
       configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook	function is 
       called if a task stack overflow is detected.  Note the system/interrupt
       stack is not checked. */
    taskDISABLE_INTERRUPTS();
    for (;;);
}

/*-----------------------------------------------------------*/

void vApplicationTickHook(void) {
    /* This function will be called by each tick interrupt if
       configUSE_TICK_HOOK is set to 1 in FreeRTOSConfig.h.  User code can be
       added here, but the tick hook is called from an interrupt context, so
       code must not attempt to block, and only the interrupt safe FreeRTOS API
       functions can be used (those that end in FromISR()). */
}

/*-----------------------------------------------------------*/

void _general_exception_handler(unsigned long ulCause, unsigned long ulStatus) {
    /* This overrides the definition provided by the kernel.  Other exceptions 
       should be handled here. */
    for (;;);
}

/*-----------------------------------------------------------*/

void vAssertCalled(const char * pcFile, unsigned long ulLine) {
    volatile unsigned long ul = 0;

    (void) pcFile;
    (void) ulLine;

    __asm volatile( "di");
    {
        /* Set ul to a non-zero value using the debugger to step out of this
           function. */
        while (ul == 0) {
            portNOP();
        }
    }
    __asm volatile( "ei");
}
